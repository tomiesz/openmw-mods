# The new version is banned in profile version 2
# See news article about the 3.0 profiles
>=common/mw-2
# Configtool 0.9 uses a different default ordering from portmod's
# builtin VFS, which may cause issues with packages that reference
# the VFS
>=modules/configtool-0.9
