# Version no longer available upstream
=items-misc/wares-2.2.2-r1

# This is a placeholder package, it cannot be installed! Consider contributing! https://portmod.gitlab.io/portmod/dev/index.html
common/placeholder
