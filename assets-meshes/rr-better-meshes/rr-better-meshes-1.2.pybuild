# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir, patch_dir

primary = "RR_-_Better_Meshes-43266-1-2"
books = "Better_Books-43266-1-0"
frescoes = "Better_Frescoes_V1.1-43266-1-1"
redoran_architecture = "Better_Redoran_Architecture-43266-1-1-1547922122"


class Package(MW):
    NAME = "RR Better Meshes"
    DESC = "Properly smoothed vanilla Morrowind meshes"
    TAGS = "TODO: FILLME"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43266"
    LICENSE = "attribution"
    DEPEND = "virtual/imagemagick"
    KEYWORDS = ""
    TIER = 1
    SRC_URI = f"""
        {primary}.rar
        Fix_for_artifact_bittercup_01-43266-.rar
        Fix_for_misc_com_metal_plate_05-43266-1-0-1551797541.rar
        Fix_for_misc_dwrv_artifact60-43266-1-0.rar
        books? ( {books}.rar )
        frescoes? ( {frescoes}.rar )
        redoran-architecture? ( {redoran_architecture}.rar )
    """

    # TODO: Better_Redoran_Architecture_-_Atlas-43266-1-1-1547922235.rar
    IUSE = """
        skooma-pipe-1
        skooma-pipe-2
        skooma-pipe-3
        potions
        books
        frescoes
        redoran-architecture
        gitd
    """
    REQUIRED_USE = "?? ( skooma-pipe-1 skooma-pipe-2 skooma-pipe-3 )"
    INSTALL_DIRS = [
        InstallDir("RR - Better Meshes/Data Files", S=primary),
        InstallDir(
            "RR - Better Meshes/Optional/0.5 Potions/Data files",
            S=primary,
            REQUIRED_USE="potions",
        ),
        InstallDir(
            "RR - Better Meshes/Optional/Skooma Pipes/3",
            S=primary,
            REQUIRED_USE="skooma-pipe-3",
        ),
        InstallDir(
            "RR - Better Meshes/Optional/Skooma Pipes/2",
            S=primary,
            REQUIRED_USE="skooma-pipe-2",
        ),
        InstallDir(
            "RR - Better Meshes/Optional/Skooma Pipes/1",
            S=primary,
            REQUIRED_USE="skooma-pipe-1",
        ),
        # TODO: InstallDir(
        #    "RR - Better Meshes/Optional/- Russian",
        #    PLUGINS=[File("RR_Apparatus_Fix_1C.ESP")],
        #    S=primary,
        # ),
        InstallDir("RR - Better Books/Data Files", S=books, REQUIRED_USE="books"),
        InstallDir(
            "RR - Better Frescoes/Data Files", S=frescoes, REQUIRED_USE="frescoes"
        ),
        InstallDir(
            "RR - Better Redoran Architecture/Main Files",
            S=redoran_architecture,
            REQUIRED_USE="redoran-architecture",
            PLUGINS=[
                File("RR_Aldruhn_Under_Skar_Eng.ESP"),
                # TODO: i10n support
                # File("RR_Aldruhn_Under_Skar_Rus.ESP"),
            ],
        ),
    ]

    def src_prepare(self):
        dwrv_artifact_path = os.path.join(
            "RR - Better Meshes",
            "Data Files",
            "Meshes",
            "m",
            "misc_dwrv_artifact60.nif",
        )
        bittercup_path = os.path.join(
            "RR - Better Meshes",
            "Data Files",
            "Meshes",
            "m",
            "artifact_bittercup_01.nif",
        )
        metal_plate_path = os.path.join(
            "RR - Better Meshes",
            "Data Files",
            "Meshes",
            "m",
            "misc_com_metal_plate_05.nif",
        )
        os.remove(dwrv_artifact_path)
        os.remove(bittercup_path)
        os.remove(metal_plate_path)

        os.rename(
            os.path.join(
                self.WORKDIR,
                "Fix_for_misc_dwrv_artifact60-43266-1-0",
                "misc_dwrv_artifact60.nif",
            ),
            dwrv_artifact_path,
        )

        os.rename(
            os.path.join(
                self.WORKDIR,
                "Fix_for_artifact_bittercup_01-43266-",
                "Fix",
                "Data Files",
                "Meshes",
                "m",
                "artifact_bittercup_01.nif",
            ),
            bittercup_path,
        )

        os.rename(
            os.path.join(
                self.WORKDIR,
                "Fix_for_misc_com_metal_plate_05-43266-1-0-1551797541",
                "misc_com_metal_plate_05.nif",
            ),
            metal_plate_path,
        )

        if "redoran-architecture" in self.USE and "gitd" in self.USE:
            source_path = os.path.join(
                self.WORKDIR,
                redoran_architecture,
                "RR - Better Redoran Architecture",
                "Main Files GITD Patch",
            )
            dest_path = os.path.join(
                self.WORKDIR,
                redoran_architecture,
                "RR - Better Redoran Architecture/Main Files",
            )
            patch_dir(source_path, dest_path)

        super().src_prepare(self)
