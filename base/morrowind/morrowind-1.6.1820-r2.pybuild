# a Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Morrowind"
    DESC = "Base morrowind data files by Bethesda"
    HOMEPAGE = "https://elderscrolls.bethesda.net/en/morrowind"
    KEYWORDS = "openmw tes3mp"
    IUSE = "bloodmoon tribunal"
    TIER = "0"
    DEPEND = ">=common/mw-2"
    RDEPEND = "base/morrowind-data"
    # Technically there is a specific EULA, however the user would
    # have accepted it already when installing the files.
    LICENSE = "all-rights-reserved"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File("Morrowind.esm"),
                File(
                    "Bloodmoon.esm", REQUIRED_USE="bloodmoon", OVERRIDES="Tribunal.esm"
                ),
                File("Tribunal.esm", REQUIRED_USE="tribunal"),
            ],
        )
    ]

    def src_install(self):
        metadata = self.get_metadata()
        metadata["path"] = os.path.join(self.ROOT, "pkg", "base", "morrowind-data")
        metadata.update(self.INSTALL_DIRS[0].dump(self))
        self.write_metadata(metadata)
