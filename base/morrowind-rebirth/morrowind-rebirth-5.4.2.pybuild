# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Morrowind Rebirth"
    DESC = "A total overhaul for morrowind"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/37795"
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        !!landmasses/tamriel-rebuilt
        !!assets-misc/morrowind-optimization-patch
        !!base/patch-for-purists
        firemoth? ( quests-misc/siege-at-fort-firemoth )
        !firemoth? ( !!quests-misc/siege-at-fort-firemoth )
        uviriths-legacy? ( quests-factions/uviriths-legacy )
        !uviriths-legacy? ( !!quests-factions/uviriths-legacy )
    """
    # See https://www.nexusmods.com/morrowind/articles/42
    # for details on compatibility
    # Also incompatible with poorly-placed-objects and texture-fix
    KEYWORDS = "openmw"
    MAIN_FILE = "Morrowind_Rebirth-37795-5-42-1619622352"
    OPT_FILE = "Morrowind_Rebirth_-_Optional_Files-37795-4-8"
    NEXUS_SRC_URI = f"""
        https://www.nexusmods.com/morrowind/mods/37795?tab=files&file_id=1000024179
        -> {MAIN_FILE}.rar
        https://www.nexusmods.com/morrowind/mods/37795?tab=files&file_id=1000011976
        -> {OPT_FILE}.rar
    """
    IUSE = """
        +races +settings +mercs +tools +skills +birthsigns
        bitter-coast west-gash rugs music menu1 menu2 splashscreens
        firemoth uviriths-legacy
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/37795"
    TEXTURE_SIZES = "2048"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            PLUGINS=[
                File("Morrowind Rebirth [Main].ESP"),
                File(
                    "Morrowind Rebirth - Mercenaries [Addon].ESP", REQUIRED_USE="mercs"
                ),
                File("Morrowind Rebirth - Tools [Addon].ESP", REQUIRED_USE="tools"),
                File("Morrowind Rebirth - Skills [Addon].ESP", REQUIRED_USE="skills"),
                File(
                    "Morrowind Rebirth - Game Settings [Addon].ESP",
                    REQUIRED_USE="settings",
                ),
                File("Morrowind Rebirth - Races [Addon].esp", REQUIRED_USE="races"),
                File(
                    "Morrowind Rebirth - Birthsigns [Addon].ESP",
                    REQUIRED_USE="birthsigns",
                ),
            ],
            S=MAIN_FILE,
        ),
        InstallDir(
            "Compability Patches",
            BLACKLIST=[
                "Julan Ashlander Companion 3.0 beta [For Rebirth].esp",
                "Julan Ashlander Companion 2.02 [For Rebirth].esp",
                "Morrowind Patch Project v1.6.6 [For Rebirth].esm",
                "Clean Main Quest Overhaul 1.3 [For Rebirth].ESP",
            ],
            PLUGINS=[
                # File("Morrowind Patch Project v1.6.6 [For Rebirth].esm"),
                # File("Julan Ashlander Companion 3.0 beta [For Rebirth].esp"),
                # File("Julan Ashlander Companion 2.02 [For Rebirth].esp"),
                # File("Clean Main Quest Overhaul 1.3 [For Rebirth].ESP"),
                File(
                    "Uvirith's Legacy_3.53 [For Rebirth].esp",
                    REQUIRED_USE="uviriths-legacy",
                ),
                File("Siege at Firemoth [For Rebirth].esp", REQUIRED_USE="firemoth"),
            ],
            S=MAIN_FILE,
        ),
        InstallDir(
            "Morrowind Rebirth - Optional Files/New Textures/Better Bitter Coast",
            S=OPT_FILE,
            REQUIRED_USE="bitter-coast",
        ),
        InstallDir(
            "Morrowind Rebirth - Optional Files/New Textures/Better West Gash",
            S=OPT_FILE,
            REQUIRED_USE="west-gash",
        ),
        InstallDir(
            "Morrowind Rebirth - Optional Files/New Textures/Better Rugs",
            S=OPT_FILE,
            REQUIRED_USE="rugs",
        ),
        InstallDir(
            "Morrowind Rebirth - Optional Files/New Music",
            S=OPT_FILE,
            REQUIRED_USE="music",
        ),
        InstallDir(
            "Morrowind Rebirth - Optional Files/New Main Menu/Alt 2",
            S=OPT_FILE,
            REQUIRED_USE="menu2",
        ),
        InstallDir(
            "Morrowind Rebirth - Optional Files/New Main Menu/Alt 1",
            S=OPT_FILE,
            REQUIRED_USE="menu1",
        ),
        InstallDir(
            "Morrowind Rebirth - Optional Files/New Splashscreens",
            S=OPT_FILE,
            REQUIRED_USE="splashscreens",
        ),
    ]
